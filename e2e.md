## Steps

1. Setup AWS credential

   Setup the AWS access key in `~/.aws/credentials` file:

   ```properties
   [default]
   aws_access_key_id = ...
   aws_secret_access_key = ...
   ```

   ​

2. Create SQS Queues

   Config `service-dashboard-backend/testtools/src/main/resources/config.json` , change the region name, bucket name, SQS queue name to suite your need. There are 3 queues to receive messages and another dead-letter queue.

   ​

   Then run `service-dashboard-backend/testtools/src/main/java/com/topcoder/dashboard/testtools/SqsS3bucketCreator.java` to create these 4 SQS queues.

   ​

3. Setup Backend App

   Use `table_create.sql` to create tables in Postgresql. Then config Postgresql host, port, database and user/password in env:

   ```Shell
   export DATABASE_SERVICE_HOST=localhost
   export DATABASE_SERVICE_PORT=5432
   export POSTGRESQL_DATABASE=postgres
   export POSTGRESQL_USER=postgres
   export POSTGRESQL_PASSWORD=postgres
   ```

   Config region, bucket and queue names in env:

   ```Shell
   export QUEUE_1_BUCKET_REGION=us-west-1

   export QUEUE_1_BUCKET_NAME=...
   export QUEUE_1_SQS_NAME=...
   export QUEUE_2_BUCKET_NAME=...
   export QUEUE_2_SQS_NAME=...
   export QUEUE_3_BUCKET_NAME=...
   export QUEUE_3_SQS_NAME=...

   export DEAD_LETTER_QUEUE_BUCKET_NAME=...
   export DEAD_LETTER_QUEUE_SQS_NAME=...

   export REPLAY_QUEUE_BUCKET_NAME=...
   export REPLAY_QUEUE_SQS_NAME=...
   ```

   **Note:**

   - **The region/bucket/queue names should be same as the names you configured in step 2**
   - **Use same bucket/queue name for DEAL_LETTER and REPLAY queues**

   ​

   Then in the `service-dashboard-backend` folder:

   ```Shell
   mvn clean package -Dmaven.test.skip=true
   java -jar sqsconsumer/target/sqsconsumer-0.0.1-SNAPSHOT.jar
   ```

   ​

4. Setup Frontend App

   In `service-dashboard-frontend` folder, config `baseUri` in `src/environments/environment.ts` to the backend api, then run:

   ```Shell
   npm i
   ```

   ​

5. Run E2E Tests

   Setup the AWS region and access key in env:

   ```Shell
   export AWS_REGION=...
   export AWS_ACCESS_KEY_ID=...
   export AWS_SECRET_ACCESS_KEY=...
   ```

   Setup the Queue 1 and Replay queue names in env (The queue names should match the names you configured in step 3):

   ```shell
   export QUEUE_1_SQS_NAME=...
   export REPLAY_QUEUE_SQS_NAME=...
   ```

   The run:

   ```shell
   npm run e2e
   ```

   ​

   ## Demo Video

   https://youtu.be/QvAunXtqR2k

   ​