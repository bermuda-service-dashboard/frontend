/*
 * Copyright (C) 2018 TopCoder Inc., All Rights Reserved.
 */
import * as assert from 'assert';
import * as _ from 'lodash';
import * as moment from 'moment';
import { browser, by, $, $$, ExpectedConditions } from 'protractor';
import { config, setupData, provider, service, data } from '../init';
import { DashboardPage } from '../po/DashboardPage';
import { AWSQueue } from '../queue/AWSQueue';

/**
 * E2E tests for the Dashboard page.
 *
 * @author TCSCODER
 * @version 1.0
 */
let page: DashboardPage;

/**
 * Format date.
 * @param date the date to format
 * @returns formatted date string
 */
function formatDate(date: string) {
  return moment(date, moment.ISO_8601).format('M/D/YY, hh:mm A');
}

/**
 * Setup the tests.
 * @param username the user name
 */
async function beforeTests(username: string) {
  await setupData();
  page = new DashboardPage(username);
  page.open();
}

/**
 * Defines the message row.
 */
interface MessageRow {
  Provider: string;
  Service?: string;
  Version?: string;
  'Sync-Async'?: string;
  Type?: string;
  UserID?: string;
  RequestingSystem?: string;
  RequestingSystemMessage?: string;
  Status?: string;
  Replay?: string;
  Timestamp?: string;
}

/**
 * Find a message row.
 * @param row the expected message row to find
 */
function findMessageRow(row: MessageRow) {

  page.clearSearch();

  // Search the message by Provider/Service/Version
  page.selectOption('Service Provider', row.Provider);
  if (row.Service) {
    page.selectOption('Service', row.Service);
  }
  if (row.Version) {
    page.selectOption('Service Version', row.Version);
  }
  if (row['Sync-Async'] === 'invalid') {
    const labels = page.findByText(page.matLabels, 'Message Attribute Name');
    const inputs = labels.get(0).all(by.xpath('../input'));
    inputs.get(0).sendKeys('transmission_type');
    inputs.get(1).sendKeys('invalid');
  }

  page.findButton('Search').click();

  // Row should exist
  expect($$('.mat-row').count()).toEqual(1);
}

describe('Dashboard page E2E tests', () => {

  describe('Test lookup data', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    it(`Lookup data of "${provider} 1" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 1`);
      page.selectOption('Service', `${service} 1`);
      page.selectOption('Service Version', 'Version 1');
      page.selectOption('Service', `${service} 2`);
      page.selectOption('Service Version', 'Version 2');
    });

    it(`Lookup data of "${provider} 2" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 2`);
      page.selectOption('Service', `${service} 4`);
      page.selectOption('Service Version', 'Version 4');
    });

    it(`Lookup data of "${provider} 3" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 3`);
      page.selectOption('Service', `${service} 5`);
      page.selectOption('Service Version', 'Version 5');
    });

    it(`Lookup data of "${provider} 4" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 4`);
      page.selectOption('Service', `${service} 6`);
      page.selectOption('Service Version', 'Version 6');
      page.selectOption('Service', `${service} 7`);
      page.selectOption('Service Version', 'Version 7');
    });

    it('Lookup data of Provider "Serv-Dash Int" should be correct', () => {
      page.clearSearch();
      page.selectOption('Service Provider', 'Serv-Dash Int');
      page.selectOption('Service', `${service} 8`);
      page.selectOption('Service Version', 'Version 8');
      page.selectOption('Service', `${service} 9`);
      page.selectOption('Service Version', 'Version 9');
    });

    it('Lookup data of "Message Type" should be correct', () => {
      page.clearSearch();
      page.selectOption('Message Type', `${service} 8`);
      page.selectOption('Message Type', `${service} 9`);
      page.selectOption('Message Type', 'request');
    });

    it('Lookup data of "Replay Y/N" should be correct', () => {
      page.clearSearch();
      page.selectOption('Replay Y/N', 'Yes');
      page.selectOption('Replay Y/N', 'No');
    });

    it('Lookup data of "Sync/Async" should be correct', () => {
      page.clearSearch();
      page.selectOption('Sync/Async', 'async');
      page.selectOption('Sync/Async', 'sync');
    });

    it('Lookup data of "Requesting System" should be correct', () => {
      page.clearSearch();
      page.selectOption('Requesting System', 'topcoder');
      page.selectOption('Requesting System', 'google');
    });

    it('Lookup data of "Status" should be correct', () => {
      page.clearSearch();
      page.selectOption('Status', 'null');
      page.selectOption('Status', 'success');
      page.selectOption('Status', 'failure');
    });
  });

  describe('Test search', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    /**
     * Test search by lookup option.
     * @param selectorName the selector name
     * @param optionValue the option value
     * @param column the column name
     * @param rowCount the expected row count
     * @param exact flag indicated whether given rowCount is exactly expected
     */
    function testSearchByLookup(selectorName: string, optionValue: string, column: string, rowCount: number, exact: boolean = false) {
      page.clearSearch();

      page.selectOption(selectorName, optionValue);

      page.testSearch(column, optionValue, rowCount, exact);
    }

    /**
     * Test search by input value.
     * @param inputName the input name
     * @param inputValue the input value
     * @param column the column name
     * @param rowCount the expected row count
     * @param exact flag indicated whether given rowCount is exactly expected
     */
    function testSearchByInput(inputName: string, inputValue: string, column: string, rowCount: number, exact: boolean = false) {
      page.clearSearch();

      page.sendInputKeys(inputName, inputValue);

      page.testSearch(column, inputValue, rowCount, exact);
    }

    /**
     * Test search by start/end date.
     * @param start the start date
     * @param end the end date
     * @param cellValue the cell value
     * @param rowCount the expected row count
     * @param exact flag indicated whether given rowCount is exactly expected
     */
    function testSearchByDateTime(start: string, end: string, cellValue: string, rowCount: number, exact: boolean = false) {
      page.clearSearch();

      page.sendDateTime('Start Date/Time', start);
      page.sendDateTime('Stop Date/Time', end);

      page.testSearch('mat-column-Timestamp', cellValue, rowCount, exact);
    }

    it(`Search by Provider "${provider} 1" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 1`, 'mat-column-Provider', 2, true);
    });

    it(`Search by Provider "${provider} 2" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 2`, 'mat-column-Provider', 2, true);
    });

    it(`Search by Provider "${provider} 3" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 3`, 'mat-column-Provider', 1, true);
    });

    it(`Search by Provider "${provider} 4" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 4`, 'mat-column-Provider', 2, true);
    });

    it('Search by Provider "Serv-Dash Int" should success', () => {
      testSearchByLookup('Service Provider', 'Serv-Dash Int', 'mat-column-Provider', 2);
    });

    it('Search by Message Type "request" should success', () => {
      testSearchByLookup('Message Type', 'request', 'mat-column-Type', 5);
    });

    it(`Search by Message Type "${service} 8" should success`, () => {
      testSearchByLookup('Message Type', `${service} 8`, 'mat-column-Type', 1, true);
    });

    it(`Search by Message Type "${service} 9" should success`, () => {
      testSearchByLookup('Message Type', `${service} 9`, 'mat-column-Type', 1, true);
    });

    it('Search by Replay "Yes" should success', () => {
      testSearchByLookup('Replay Y/N', 'Yes', 'mat-column-Replay', 2);
    });

    it('Search by Replay "No" should success', () => {
      testSearchByLookup('Replay Y/N', 'No', 'mat-column-Replay', 4);
    });

    it('Search by Sync should success', () => {
      testSearchByLookup('Sync/Async', 'sync', 'mat-column-Sync-Async', 1);
    });

    it('Search by should success', () => {
      testSearchByLookup('Sync/Async', 'async', 'mat-column-Sync-Async', 5);
    });

    it('Search by Requesting System "topcoder" should success', () => {
      testSearchByLookup('Requesting System', 'topcoder', 'mat-column-RequestingSystem', 4);
    });

    it('Search by Requesting System "google" should success', () => {
      testSearchByLookup('Requesting System', 'google', 'mat-column-RequestingSystem', 2);
    });

    it('Search by Status "success" should success', () => {
      testSearchByLookup('Status', 'success', 'mat-column-Status', 3);
    });

    it('Search by Status "failure" should success', () => {
      testSearchByLookup('Status', 'failure', 'mat-column-Status', 2);
    });

    it('Search by Status "null" should success', () => {
      testSearchByLookup('Status', 'null', 'mat-column-Status', 3);
    });

    it('Search by Requesting System Message ID "334" should success', () => {
      testSearchByInput('Requesting System Message ID', '334', 'mat-column-RequestingSystemMessage', 2);
    });

    it('Search by Requesting System Message ID "1212" should success', () => {
      testSearchByInput('Requesting System Message ID', '1212', 'mat-column-RequestingSystemMessage', 4);
    });

    it('Search by User ID "1212" should success', () => {
      testSearchByInput('User ID', '1212', 'mat-column-UserID', 4);
    });

    it('Search by User ID "3234234" should success', () => {
      testSearchByInput('User ID', '3234234', 'mat-column-UserID', 2);
    });

    it('Search by Date/Time "01/01/2018 00:00 - 01/01/2018 23:00" should success', () => {
      testSearchByDateTime('01/01/2018 00:00', '01/01/2018 23:00', formatDate('2018-01-01'), 4);
    });

    it('Search by Date/Time "02/01/2018 00:00 - 02/01/2018 23:00" should success', () => {
      testSearchByDateTime('02/01/2018 00:00', '02/01/2018 23:00', formatDate('2018-01-02'), 2);
    });

    it('Search by Date/Time "27/01/2018 00:00 - 28/01/2018 00:00" should success', () => {
      testSearchByDateTime('27/01/2018 00:00', '28/01/2018 00:00', formatDate('2018-01-27T04:12:52Z'), 2);
    });

    it('Search by Correlation ID "c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11" should success', () => {
      page.clearSearch();

      page.sendInputKeys('Correlation ID', 'c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11');
      page.findButton('Search').click();

      const rows = $$('mat-row');
      expect(rows.count()).toBeGreaterThanOrEqual(2);
    });

    it('Search by Message Attribute "message_type:request" should success', () => {
      page.clearSearch();
      const labels = page.findByText(page.matLabels, 'Message Attribute Name');

      const inputs = labels.get(0).all(by.xpath('../input'));
      inputs.get(0).sendKeys('message_type');
      inputs.get(1).sendKeys('request');

      page.testSearch('mat-column-Type', 'request', 5);
    });

    it('Search by Message Attribute "message_type:request, status=success" should success', () => {
      page.clearSearch();
      const labels = page.findByText(page.matLabels, 'Message Attribute Name');

      let inputs = labels.get(0).all(by.xpath('../input'));
      inputs.get(0).sendKeys('message_type');
      inputs.get(1).sendKeys('request');

      inputs = labels.get(1).all(by.xpath('../input'));
      inputs.get(0).sendKeys('status');
      inputs.get(1).sendKeys('success');

      page.testSearch('mat-column-Type', 'request', 3);
      page.testSearch('mat-column-Status', 'success', 3);
    });

    it('Search by Message Attribute "message_type:request, status=success, transmission_type=async" should success', () => {
      page.clearSearch();

      page.findButton('Add More').click();

      const labels = page.findByText(page.matLabels, 'Message Attribute Name');

      let inputs = labels.get(0).all(by.xpath('../input'));
      inputs.get(0).sendKeys('message_type');
      inputs.get(1).sendKeys('request');

      inputs = labels.get(1).all(by.xpath('../input'));
      inputs.get(0).sendKeys('status');
      inputs.get(1).sendKeys('success');

      inputs = labels.get(2).all(by.xpath('../input'));
      inputs.get(0).sendKeys('transmission_type');
      inputs.get(1).sendKeys('async');

      page.testSearch('mat-column-Type', 'request', 2);
      page.testSearch('mat-column-Status', 'success', 2);
      page.testSearch('mat-column-Sync-Async', 'async', 2);
    });
  });

  describe('Test sort', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    it('Sort by Provider should be correct', () => {
      page.testSort('Provider');
    });

    it('Sort by Service should be correct', () => {
      page.testSort('Service');
    });

    it('Sort by Version should be correct', () => {
      page.testSort('Version');
    });

    it('Sort by Sync-Async should be correct', () => {
      page.testSort('Sync-Async');
    });

    it('Sort by Type should be correct', () => {
      page.testSort('Type');
    });

    it('Sort by UserID should be correct', () => {
      page.testSort('UserID');
    });

    it('Sort by RequestingSystemMessage should be correct', () => {
      page.testSort('RequestingSystemMessage');
    });

    it('Sort by Status should be correct', () => {
      page.testSort('Status');
    });

    it('Sort by Replay should be correct', () => {
      page.testSort('Replay');
    });

    it('Sort by Timestamp should be correct', () => {
      page.testSort('Timestamp');
    });
  });

  describe('Test message details', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    /**
     * Test message detail.
     * @param row the expected message row
     * @param detail the expected message detail
     */
    function testMessageDetail(row: MessageRow, detail: any) {

      // Close the message detail dialog if present
      const displayDialog = $('app-display-message');
      displayDialog.isPresent().then((present) => {
        if (present) {
          page.findButton('Close').click();
        }
      }).catch(() => { });
      browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(displayDialog)), 5000,
        'Message detail dialog takes too long to disappear');

      findMessageRow(row);

      // Assert row texts
      for (const key of Object.keys(row)) {
        const value = row[key];
        if (value === null) {
          expect($(`mat-cell.mat-column-${key}`).getText()).toEqual('');
        } else {
          expect($(`mat-cell.mat-column-${key}`).getText()).toEqual(value);
        }
      }

      // Display message details
      $('mat-cell mat-checkbox').click();
      page.findButton('Display Message').click();
      browser.wait(ExpectedConditions.visibilityOf(displayDialog), 5000, 'Message detail dialog takes too long to appear');

      // Assert message details
      $('app-display-message code').getText().then(txt => {
        const detailObj = JSON.parse(txt);

        assert(detailObj.messageId, 'message id should be present');
        assert(detailObj.transactionId, 'transaction id should be present');
        assert(detailObj.createdBy, 'created by should be present');
        assert(detailObj.createdDate, 'created date should be present');

        // Delete these data as they are backend db generated and will change in time
        delete detailObj.messageId;
        delete detailObj.transactionId;
        delete detailObj.createdBy;
        delete detailObj.createdDate;
        delete detailObj.typeId;
        delete detailObj.messageTypeId;
        delete detailObj.requestingSystemId;

        if (!detail.replayEndpoint) {
          delete detailObj.replayEndpoint;
          delete detailObj.replayEndpointType;
        }

        // Allow null and undefined equal
        function assertNull(obj1: any, obj2: any) {
          for (const key of Object.keys(obj1)) {
            if (obj1[key] === null) {
              if (obj2[key] !== null && obj2[key] !== undefined) {
                assert.fail(obj1[key], obj2[key]);
              } else {
                obj2[key] = null;
              }
            }
          }
        }
        assertNull(detailObj, detail);
        assertNull(detail, detailObj);

        // Parse dates within bmessage
        function parseDates(obj) {
          _.forOwn(obj, (value, key) => {
            if (typeof value === 'object') {
              obj[key] = parseDates(value);
            }

            if (/date|timestamp/i.test(key)) {
              obj[key] = (new Date(obj[key])).toISOString();
            }
          });

          return obj;
        }
        detail.bmessage = parseDates(_.cloneDeep(detail.bmessage));

        // Deep equal assertion
        assert.deepEqual(detailObj, detail, 'Message detail is incorrect');
      });
    }

    /**
     * Convert message row to messge detial.
     * @param expectedRow the expected message row
     * @param timestamp the message timestamp
     * @param bmessage the bmessage
     * @returns messge detial
     */
    function convertMessageRowToDetail(expectedRow: MessageRow, timestamp: string, bmessage: any) {
      return {
        serviceProvider: expectedRow.Provider,
        service: expectedRow.Service,
        serviceVersion: expectedRow.Version,
        version: expectedRow.Version,
        timestamp: moment(timestamp, moment.ISO_8601).toISOString(),
        processingTimestamp: moment(timestamp, moment.ISO_8601).toISOString(),
        statusCd: expectedRow.Status,
        replay: expectedRow.Replay,
        userId: expectedRow.UserID,
        transmissionType: expectedRow['Sync-Async'],
        requestingSystem: expectedRow.RequestingSystem,
        messageType: expectedRow.Type,
        requestingSystemMessageId: expectedRow.RequestingSystemMessage,
        selected: true,
        bmessage
      };
    }

    it('sns message detail should be correct', () => {
      const sns = data.sns;
      const expectedRow = {
        Provider: sns.header.publishing_system,
        Service: sns.header.message_name,
        Version: sns.header.message_version,
        'Sync-Async': null,
        Type: null,
        UserID: null,
        RequestingSystem: null,
        RequestingSystemMessage: null,
        Status: 'null',
        Replay: 'No',
        Timestamp: formatDate(sns.header.message_date)
      };
      testMessageDetail(expectedRow, convertMessageRowToDetail(expectedRow, sns.header.message_date, sns));
    });

    it('sns_invalidjson message detail should be correct', () => {
      const sns_invalidjson = data.sns_invalidjson;
      const expectedRow = {
        Provider: sns_invalidjson.header.publishing_system,
        Service: sns_invalidjson.header.message_name,
        Version: sns_invalidjson.header.message_version,
        'Sync-Async': null,
        Type: null,
        UserID: null,
        RequestingSystem: null,
        RequestingSystemMessage: null,
        Status: 'null',
        Replay: 'No',
        Timestamp: formatDate(sns_invalidjson.header.message_date)
      };
      testMessageDetail(expectedRow, convertMessageRowToDetail(expectedRow, sns_invalidjson.header.message_date, sns_invalidjson));
    });

    it('sqs_async message detail should be correct', () => {
      const sqs_async = data.sqs_async;
      const expectedRow = {
        Provider: sqs_async.dashboard_wrapper.service_provider_name,
        Service: sqs_async.dashboard_wrapper.message.header.service_name,
        Version: sqs_async.dashboard_wrapper.message.header.service_version,
        'Sync-Async': sqs_async.dashboard_wrapper.transmission_type,
        Type: sqs_async.dashboard_wrapper.message_type,
        UserID: sqs_async.dashboard_wrapper.message.header.user_id,
        RequestingSystem: sqs_async.dashboard_wrapper.message.header.requesting_system,
        RequestingSystemMessage: sqs_async.dashboard_wrapper.message.header.requesting_system_message_id,
        Status: sqs_async.dashboard_wrapper.status,
        Replay: 'No',
        Timestamp: formatDate(sqs_async.dashboard_wrapper.timestamp)
      };
      testMessageDetail(expectedRow, {
        ...convertMessageRowToDetail(expectedRow, sqs_async.dashboard_wrapper.timestamp, sqs_async),
        replayMessage: sqs_async.dashboard_wrapper.message.header.replay_message,
        coorelationUuid: sqs_async.dashboard_wrapper.message.header.coorelation_id,
        replayEndpointType: sqs_async.dashboard_wrapper.replay_endpoint_type,
        replayEndpoint: sqs_async.dashboard_wrapper.replay_endpoint
      });
    });

    it('sqs_empty_uuid message detail should be correct', () => {
      const sqs_empty_uuid = data.sqs_empty_uuid;
      const expectedRow = {
        Provider: sqs_empty_uuid.dashboard_wrapper.service_provider_name,
        Service: sqs_empty_uuid.dashboard_wrapper.message.header.service_name,
        Version: sqs_empty_uuid.dashboard_wrapper.message.header.service_version,
        'Sync-Async': sqs_empty_uuid.dashboard_wrapper.transmission_type,
        Type: sqs_empty_uuid.dashboard_wrapper.message_type,
        UserID: sqs_empty_uuid.dashboard_wrapper.message.header.user_id,
        RequestingSystem: sqs_empty_uuid.dashboard_wrapper.message.header.requesting_system,
        RequestingSystemMessage: sqs_empty_uuid.dashboard_wrapper.message.header.requesting_system_message_id,
        Status: sqs_empty_uuid.dashboard_wrapper.status,
        Replay: 'No',
        Timestamp: formatDate(sqs_empty_uuid.dashboard_wrapper.timestamp)
      };
      testMessageDetail(expectedRow, {
        ...convertMessageRowToDetail(expectedRow, sqs_empty_uuid.dashboard_wrapper.timestamp, sqs_empty_uuid),
        replayMessage: sqs_empty_uuid.dashboard_wrapper.message.header.replay_message,
        coorelationUuid: null,
        replayEndpointType: sqs_empty_uuid.dashboard_wrapper.replay_endpoint_type,
        replayEndpoint: sqs_empty_uuid.dashboard_wrapper.replay_endpoint
      });
    });

    it('sqs_invalid_tranmission_type message detail should be correct', () => {
      const sqs_invalid_tranmission_type = data.sqs_invalid_tranmission_type;
      const expectedRow = {
        Provider: sqs_invalid_tranmission_type.dashboard_wrapper.service_provider_name,
        Service: null,
        Version: null,
        'Sync-Async': sqs_invalid_tranmission_type.dashboard_wrapper.transmission_type,
        Type: sqs_invalid_tranmission_type.dashboard_wrapper.message_type,
        UserID: null,
        RequestingSystem: null,
        RequestingSystemMessage: null,
        Status: sqs_invalid_tranmission_type.dashboard_wrapper.status,
        Replay: 'No',
        Timestamp: formatDate(sqs_invalid_tranmission_type.dashboard_wrapper.timestamp)
      };
      testMessageDetail(expectedRow, {
        ...convertMessageRowToDetail(expectedRow, sqs_invalid_tranmission_type.dashboard_wrapper.timestamp,
          sqs_invalid_tranmission_type),
        replayEndpointType: sqs_invalid_tranmission_type.dashboard_wrapper.replay_endpoint_type,
        replayEndpoint: sqs_invalid_tranmission_type.dashboard_wrapper.replay_endpoint
      });
    });

    it('sqs_invalidstatus message detail should be correct', () => {
      const sqs_invalidstatus = data.sqs_invalidstatus;
      const expectedRow = {
        Provider: sqs_invalidstatus.dashboard_wrapper.service_provider_name,
        Service: sqs_invalidstatus.dashboard_wrapper.message.header.service_name,
        Version: sqs_invalidstatus.dashboard_wrapper.message.header.service_version,
        'Sync-Async': sqs_invalidstatus.dashboard_wrapper.transmission_type,
        Type: sqs_invalidstatus.dashboard_wrapper.message_type,
        UserID: sqs_invalidstatus.dashboard_wrapper.message.header.user_id,
        RequestingSystem: sqs_invalidstatus.dashboard_wrapper.message.header.requesting_system,
        RequestingSystemMessage: sqs_invalidstatus.dashboard_wrapper.message.header.requesting_system_message_id,
        Status: null,
        Replay: 'No',
        Timestamp: formatDate(sqs_invalidstatus.dashboard_wrapper.timestamp)
      };
      testMessageDetail(expectedRow, {
        ...convertMessageRowToDetail(expectedRow, sqs_invalidstatus.dashboard_wrapper.timestamp, sqs_invalidstatus),
        replayMessage: sqs_invalidstatus.dashboard_wrapper.message.header.replay_message,
        coorelationUuid: sqs_invalidstatus.dashboard_wrapper.message.header.coorelation_id,
        replayEndpointType: sqs_invalidstatus.dashboard_wrapper.replay_endpoint_type,
        replayEndpoint: sqs_invalidstatus.dashboard_wrapper.replay_endpoint
      });
    });

    it('sqs_sync message detail should be correct', () => {
      const sqs_sync = data.sqs_sync;
      const expectedRow = {
        Provider: sqs_sync.dashboard_wrapper.service_provider_name,
        Service: sqs_sync.dashboard_wrapper.service_name,
        Version: sqs_sync.dashboard_wrapper.service_version,
        'Sync-Async': sqs_sync.dashboard_wrapper.transmission_type,
        Type: sqs_sync.dashboard_wrapper.message_type,
        UserID: sqs_sync.dashboard_wrapper.user_id,
        RequestingSystem: sqs_sync.dashboard_wrapper.requesting_system,
        RequestingSystemMessage: sqs_sync.dashboard_wrapper.requesting_system_message_id,
        Status: sqs_sync.dashboard_wrapper.status,
        Replay: 'No',
        Timestamp: formatDate(sqs_sync.dashboard_wrapper.timestamp)
      };
      testMessageDetail(expectedRow, {
        ...convertMessageRowToDetail(expectedRow, sqs_sync.dashboard_wrapper.timestamp, sqs_sync),
        replayMessage: sqs_sync.dashboard_wrapper.replay_message,
        coorelationUuid: sqs_sync.dashboard_wrapper.coorelation_id,
        replayEndpointType: sqs_sync.dashboard_wrapper.replay_endpoint_type,
        replayEndpoint: sqs_sync.dashboard_wrapper.replay_endpoint
      });
    });

    it('sqs_sery_dash_int message detail should be correct', () => {
      const sqs_sery_dash_int = data.sqs_sery_dash_int;
      const expectedRow = {
        Provider: sqs_sery_dash_int.dashboard_wrapper.service_provider_name,
        Service: sqs_sery_dash_int.dashboard_wrapper.message_type,
        Version: sqs_sery_dash_int.dashboard_wrapper.service_version,
        'Sync-Async': sqs_sery_dash_int.dashboard_wrapper.transmission_type,
        Type: sqs_sery_dash_int.dashboard_wrapper.message_type,
        UserID: sqs_sery_dash_int.dashboard_wrapper.user_id,
        RequestingSystem: sqs_sery_dash_int.dashboard_wrapper.source_system,
        RequestingSystemMessage: sqs_sery_dash_int.dashboard_wrapper.source_system_transaction_id,
        Status: sqs_sery_dash_int.dashboard_wrapper.status,
        Replay: 'Yes',
        Timestamp: formatDate(sqs_sery_dash_int.dashboard_wrapper.timestamp)
      };
      testMessageDetail(expectedRow, {
        ...convertMessageRowToDetail(expectedRow, sqs_sery_dash_int.dashboard_wrapper.timestamp, sqs_sery_dash_int),
        replayMessage: sqs_sery_dash_int.dashboard_wrapper.replay_message,
        coorelationUuid: sqs_sery_dash_int.dashboard_wrapper.coorelation_id,
        replayEndpointType: sqs_sery_dash_int.dashboard_wrapper.replay_endpoint_type,
        replayEndpoint: sqs_sery_dash_int.dashboard_wrapper.replay_endpoint
      });
    });

    it('sqs_sery_dash_int_nojson message detail should be correct', () => {
      const sqs_sery_dash_int_nojson = data.sqs_sery_dash_int_nojson;
      const expectedRow = {
        Provider: sqs_sery_dash_int_nojson.dashboard_wrapper.service_provider_name,
        Service: sqs_sery_dash_int_nojson.dashboard_wrapper.message_type,
        Version: sqs_sery_dash_int_nojson.dashboard_wrapper.service_version,
        'Sync-Async': sqs_sery_dash_int_nojson.dashboard_wrapper.transmission_type,
        Type: sqs_sery_dash_int_nojson.dashboard_wrapper.message_type,
        UserID: sqs_sery_dash_int_nojson.dashboard_wrapper.user_id,
        RequestingSystem: sqs_sery_dash_int_nojson.dashboard_wrapper.source_system,
        RequestingSystemMessage: sqs_sery_dash_int_nojson.dashboard_wrapper.source_system_transaction_id,
        Status: sqs_sery_dash_int_nojson.dashboard_wrapper.status,
        Replay: 'Yes',
        Timestamp: formatDate(sqs_sery_dash_int_nojson.dashboard_wrapper.timestamp)
      };
      testMessageDetail(expectedRow, {
        ...convertMessageRowToDetail(expectedRow, sqs_sery_dash_int_nojson.dashboard_wrapper.timestamp, sqs_sery_dash_int_nojson),
        replayMessage: sqs_sery_dash_int_nojson.dashboard_wrapper.replay_message,
        coorelationUuid: sqs_sery_dash_int_nojson.dashboard_wrapper.coorelation_id,
        replayEndpointType: sqs_sery_dash_int_nojson.dashboard_wrapper.replay_endpoint_type,
        replayEndpoint: sqs_sery_dash_int_nojson.dashboard_wrapper.replay_endpoint
      });
    });

    it('Readonly user should not see "Replay Selected" button', () => {
      expect(page.findButton('Replay Selected').isPresent()).toBeFalsy();
    });
  });

  describe('Test replay message', () => {
    const deadQueue = new AWSQueue(config.deadQueueName, config.regionName);

    beforeAll(async () => {
      await beforeTests(config.REPLAY_USER);
      await deadQueue.purgeQueue();
    });

    /**
     * Test replay.
     * @param row the message row to replay
     */
    function testReplay(row: MessageRow, message: any) {
      // Find the row
      findMessageRow(row);

      // Open replay dialog
      $('mat-cell mat-checkbox').click();
      page.findButton('Replay Selected').click();

      // Wait for replay popup appear
      browser.wait(ExpectedConditions.visibilityOf($('app-replay-popup')), 5000,
        'Replay popup takes too long to appear');

      // Input replay endpoint and type
      const inputs = $$('app-replay-popup input');

      inputs.each((input, idx) => {
        input.getAttribute('value').then((val) => {
          if (!val) {
            if (idx === 0) {
              input.sendKeys('api.topcoder.com');
            }
            if (idx === 1) {
              input.sendKeys('SQS');
            }
          }
        });
      });

      // Send the replay
      page.findButton('Send').click();

      // Wait some time
      browser.driver.sleep(1000).then(async () => {
        const messages = await deadQueue.receiveMessage();
        assert(messages && messages.length, 'Message should be replayed to dead queue');
        const messageInQueue = messages[messages.length - 1];
        assert.deepEqual(message, JSON.parse(messageInQueue.Body));
        await deadQueue.deleteMessage(messageInQueue.ReceiptHandle);
      });
    }

    it('sns should be replayed', () => {
      const message = data.sns;
      const row = {
        Provider: message.header.publishing_system,
        Service: message.header.message_name,
        Version: message.header.message_version
      };
      testReplay(row, message);
    });

    it('sns_invalidjson should be replayed', () => {
      const message = data.sns_invalidjson;
      const row = {
        Provider: message.header.publishing_system,
        Service: message.header.message_name,
        Version: message.header.message_version
      };
      testReplay(row, message);
    });

    it('sqs_async should be replayed', () => {
      const message = data.sqs_async;
      const row = {
        Provider: message.dashboard_wrapper.service_provider_name,
        Service: message.dashboard_wrapper.message.header.service_name,
        Version: message.dashboard_wrapper.message.header.service_version
      };
      testReplay(row, message);
    });

    it('sqs_empty_uuid should be replayed', () => {
      const message = data.sqs_empty_uuid;
      const row = {
        Provider: message.dashboard_wrapper.service_provider_name,
        Service: message.dashboard_wrapper.message.header.service_name,
        Version: message.dashboard_wrapper.message.header.service_version
      };
      testReplay(row, message);
    });

    it('sqs_invalidstatus should be replayed', () => {
      const message = data.sqs_invalidstatus;
      const row = {
        Provider: message.dashboard_wrapper.service_provider_name,
        Service: message.dashboard_wrapper.message.header.service_name,
        Version: message.dashboard_wrapper.message.header.service_version
      };
      testReplay(row, message);
    });

    it('sqs_invalid_tranmission_type should be replayed', () => {
      const message = data.sqs_invalid_tranmission_type;
      const row = {
        Provider: message.dashboard_wrapper.service_provider_name,
        'Sync-Async': message.dashboard_wrapper.transmission_type
      };
      testReplay(row, message);
    });

    it('sqs_sync should be replayed', () => {
      const message = data.sqs_sync;
      const row = {
        Provider: message.dashboard_wrapper.service_provider_name,
        Service: message.dashboard_wrapper.service_name,
        Version: message.dashboard_wrapper.service_version
      };
      testReplay(row, message);
    });

    it('sqs_sery_dash_int should be replayed', () => {
      const message = data.sqs_sery_dash_int;
      const row = {
        Provider: 'Serv-Dash Int',
        Service: message.dashboard_wrapper.message_type,
        Version: message.dashboard_wrapper.service_version
      };
      testReplay(row, message);
    });

    it('sqs_sery_dash_int_nojson should be replayed', () => {
      const message = data.sqs_sery_dash_int_nojson;
      const row = {
        Provider: 'Serv-Dash Int',
        Service: message.dashboard_wrapper.message_type,
        Version: message.dashboard_wrapper.service_version
      };
      testReplay(row, message);
    });
  });
});
