/*
 * Copyright (C) 2018 TopCoder Inc., All Rights Reserved.
 */
import * as assert from 'assert';
import { browser, by, $, $$, ExpectedConditions } from 'protractor';
import { config, setupData, provider, service, version, data } from '../init';
import { ServicePage } from '../po/ServicePage';

/**
 * E2E tests for the Service page.
 *
 * @author TCSCODER
 * @version 1.0
 */
let page: ServicePage;

/**
 * Setup the tests.
 * @param username the user name
 */
async function beforeTests(username: string) {
  await setupData();
  page = new ServicePage(username);
  page.open();
}

describe('Service page E2E tests', () => {

  describe('Test lookup data', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    it(`Lookup data of "${provider} 1" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 1`);
      page.selectOption('Service', `${service} 1`);
      page.selectOption('Service Version', 'Version 1');
      page.selectOption('Service', `${service} 2`);
      page.selectOption('Service Version', 'Version 2');
    });

    it(`Lookup data of "${provider} 2" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 2`);
      page.selectOption('Service', `${service} 4`);
      page.selectOption('Service Version', 'Version 4');
    });

    it(`Lookup data of "${provider} 3" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 3`);
      page.selectOption('Service', `${service} 5`);
      page.selectOption('Service Version', 'Version 5');
    });

    it(`Lookup data of "${provider} 4" should be correct`, () => {
      page.clearSearch();
      page.selectOption('Service Provider', `${provider} 4`);
      page.selectOption('Service', `${service} 6`);
      page.selectOption('Service Version', 'Version 6');
      page.selectOption('Service', `${service} 7`);
      page.selectOption('Service Version', 'Version 7');
    });

    it('Lookup data of Provider "Serv-Dash Int" should be correct', () => {
      page.clearSearch();
      page.selectOption('Service Provider', 'Serv-Dash Int');
      page.selectOption('Service', `${service} 8`);
      page.selectOption('Service Version', 'Version 8');
      page.selectOption('Service', `${service} 9`);
      page.selectOption('Service Version', 'Version 9');
    });
  });

  describe('Test search', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    /**
     * Test search by lookup option.
     * @param selectorName the selector name
     * @param optionValue the option value
     * @param column the column name
     * @param rowCount the expected row count
     * @param exact flag indicated whether given rowCount is exactly expected
     */
    function testSearchByLookup(selectorName: string, optionValue: string, column: string, rowCount: number, exact: boolean = false) {
      page.clearSearch();

      page.selectOption(selectorName, optionValue);

      page.testSearch(column, optionValue, rowCount, exact);
    }

    it(`Search by Provider "${provider} 1" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 1`, 'mat-column-Provider', 2, true);
    });

    it(`Search by Provider "${provider} 2" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 2`, 'mat-column-Provider', 1, true);
    });

    it(`Search by Provider "${provider} 3" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 3`, 'mat-column-Provider', 1, true);
    });

    it(`Search by Provider "${provider} 4" should success`, () => {
      testSearchByLookup('Service Provider', `${provider} 4`, 'mat-column-Provider', 2, true);
    });

    it('Search by Provider "Serv-Dash Int" should success', () => {
      testSearchByLookup('Service Provider', 'Serv-Dash Int', 'mat-column-Provider', 2);
    });
  });

  describe('Test sort', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    it('Sort by Provider should be correct', () => {
      page.testSort('Provider');
    });

    it('Sort by Service should be correct', () => {
      page.testSort('Service');
    });

    it('Sort by Version should be correct', () => {
      page.testSort('Version');
    });
  });

  describe('Test row details', () => {

    beforeAll(() => beforeTests(config.READONLY_USER));

    /**
     * Test row details.
     * @param row the expected row details
     */
    function testRowDetails(row) {
      page.clearSearch();
      page.selectOption('Service Provider', row.Provider);
      page.selectOption('Service', row.Service);
      page.selectOption('Service Version', row.Version);

      page.findButton('Search').click();

      // Row should exist
      expect($$('.mat-row').count()).toEqual(1);

      // Assert row texts
      expect($('mat-cell.mat-column-Provider').getText()).toEqual(row.Provider);
      expect($('mat-cell.mat-column-Service').getText()).toEqual(row.Service);
      expect($('mat-cell.mat-column-Version').getText()).toEqual(row.Version);
    }

    it(`Detail of "${provider} 1", "${service} 1", "Version 1" should be correct`, () => {
      testRowDetails({
        Provider: `${provider} 1`,
        Service: `${service} 1`,
        Version: 'Version 1'
      });
    });

    it(`Detail of "${provider} 1", "${service} 2", "Version 2" should be correct`, () => {
      testRowDetails({
        Provider: `${provider} 1`,
        Service: `${service} 2`,
        Version: 'Version 2'
      });
    });

    it(`Detail of "${provider} 2", "${service} 4", "Version 4" should be correct`, () => {
      testRowDetails({
        Provider: `${provider} 2`,
        Service: `${service} 4`,
        Version: 'Version 4'
      });
    });

    it(`Detail of "${provider} 3", "${service} 5", "Version 5" should be correct`, () => {
      testRowDetails({
        Provider: `${provider} 3`,
        Service: `${service} 5`,
        Version: 'Version 5'
      });
    });

    it(`Detail of "${provider} 4", "${service} 6", "Version 6" should be correct`, () => {
      testRowDetails({
        Provider: `${provider} 4`,
        Service: `${service} 6`,
        Version: 'Version 6'
      });
    });

    it(`Detail of "${provider} 4", "${service} 7", "Version 7" should be correct`, () => {
      testRowDetails({
        Provider: `${provider} 4`,
        Service: `${service} 7`,
        Version: 'Version 7'
      });
    });

    it(`Detail of "Serv-Dash Int", "${service} 8", "Version 8" should be correct`, () => {
      testRowDetails({
        Provider: 'Serv-Dash Int',
        Service: `${service} 8`,
        Version: 'Version 8'
      });
    });

    it(`Detail of "Serv-Dash Int", "${service} 9", "Version 9" should be correct`, () => {
      testRowDetails({
        Provider: 'Serv-Dash Int',
        Service: `${service} 9`,
        Version: 'Version 9'
      });
    });
  });
});
