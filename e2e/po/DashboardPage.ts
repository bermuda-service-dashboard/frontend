/*
 * Copyright (C) 2018 TopCoder Inc., All Rights Reserved.
 */
import { BasePage } from './BasePage';

/**
 * Dashboard page class.
 *
 * @author TCSCODER
 * @version 1.0
 */
export class DashboardPage extends BasePage {

  /**
   * Constructor with user name.
   * @param username the user name
   */
  constructor(username: string) {
    super(`/dashboard?username=${username}`);
  }
}
