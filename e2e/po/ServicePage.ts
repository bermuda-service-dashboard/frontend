/*
 * Copyright (C) 2018 TopCoder Inc., All Rights Reserved.
 */
import { BasePage } from './BasePage';

/**
 * Service page class.
 *
 * @author TCSCODER
 * @version 1.0
 */
export class ServicePage extends BasePage {

  /**
   * Constructor with user name.
   * @param username the user name
   */
  constructor(username: string) {
    super(`/serviceSearch?username=${username}`);
  }
}
