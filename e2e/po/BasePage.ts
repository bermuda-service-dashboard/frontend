/*
 * Copyright (C) 2018 TopCoder Inc., All Rights Reserved.
 */
import * as assert from 'assert';
import * as moment from 'moment';
import { browser, by, element, $, $$, ExpectedConditions, ElementFinder, ElementArrayFinder } from 'protractor';

/**
 * Base page class defines the commond methods to access web elements.
 *
 * @author TCSCODER
 * @version 1.0
 */
export class BasePage {

  /**
   * The page url.
   */
  private url: string;

  /**
   * The mat labels.
   */
  public matLabels = element.all(by.tagName('mat-label'));

  /**
   * Constructor with url.
   * @param url the page url
   */
  constructor(url) {
    this.url = url;
  }

  /**
   * Open page.
   */
  open() {
    browser.get(this.url);
  }

  /**
   * Close toast info panel.
   */
  closeToast() {
    const toast = $('.toast-close-button');
    toast.isDisplayed().then((displayed) => {
      if (displayed) {
        $('.toast-close-button').click();
      }
    }).catch(() => {});

    // Wait for toast disappear
    browser.wait(ExpectedConditions.not(ExpectedConditions.visibilityOf(toast)), 5000,
      'Toast takes too long to disappear');
  }

  /**
   * Find link.
   * @param linkText the link text
   * @returns link
   */
  findLink(linkText: string): ElementFinder {
    return element(by.linkText(linkText));
  }

  /**
   * Find button.
   * @param buttonText the button text
   * @returns button
   */
  findButton(buttonText: string): ElementFinder {
    return element(by.buttonText(buttonText));
  }

  /**
   * Find element by text.
   * @param elements the elements array
   * @param text the text of element to find
   * @returns elements found
   */
  findByText(elements: ElementArrayFinder, text: string): ElementArrayFinder {
    return elements.filter(elem => elem.getText().then(txt => txt === text));
  }

  /**
   * Find exact one element by text.
   * @param elements the elements array
   * @param text the text of element to find
   * @returns element found
   */
  findOneByText(elements: ElementArrayFinder, text: string): ElementFinder {
    const found = this.findByText(elements, text);

    found.then(arr => {
      if (!arr.length) {
        throw new Error(`Element with text is not found: ${text}`);
      } else if (arr.length > 1) {
        console.warn(`Multiple elements found with text: ${text}`);
      }
    });

    return found.first();
  }

  /**
   * Assert element's text.
   * @param elem the element
   * @param text the expected text of element
   */
  assertElementText(elem: ElementFinder, text: string) {
    expect(elem.getText()).toEqual(text);
  }

  /**
   * Select option.
   * @param selectorName the selector name
   * @param optionText the text of option to select
   */
  selectOption(selectorName: string, optionText: string) {
    // Click selector
    const matLabel = this.findOneByText(this.matLabels, selectorName);
    const selector = matLabel.element(by.xpath('../mat-select'));
    selector.click();

    // Wait for the options menu appears
    browser.wait(ExpectedConditions.presenceOf($('span.mat-option-text')), 5000, 'Options menu takes too long to appear');

    // Click option
    this.findOneByText($$('span.mat-option-text'), optionText).click();

    // Verify the option is selected
    this.assertElementText(selector.$('.mat-select-value'), optionText);
    browser.driver.sleep(200);
  }

  /**
   * Send input keys.
   * @param inputName the input name
   * @param inputValue the input value to send
   */
  sendInputKeys(inputName: string, inputValue: string) {
    const matLabel = this.findOneByText(this.matLabels, inputName);
    const input = matLabel.element(by.xpath('../input'));
    input.sendKeys(inputValue);
  }

  /**
   * Send date time.
   * @param inputName the input name
   * @param inputValue the input value to send
   */
  sendDateTime(inputName: string, inputValue: string) {
    const matLabel = this.findOneByText(this.matLabels, inputName);
    const input = matLabel.element(by.xpath('../mat-form-field')).$('input.md2-datepicker-value');
    input.sendKeys(inputValue);
  }

  /**
   * Clear search.
   */
  clearSearch() {
    // Get rid of overlay if it is displayed
    const overlay = $$('.cdk-overlay-backdrop');
    overlay.isDisplayed().then((displayed) => {
      if (displayed) {
        overlay.click();
      }
    }).catch(() => {});

    // Close toast info panel
    this.closeToast();

    this.findButton('Clear').click();
  }

  /**
   * Test search.
   * @param column the column name
   * @param cellValue the expected cell value
   * @param rowCount the expected row count
   * @param exact flag indicated whether given rowCount is exactly expected
   */
  testSearch(column: string, cellValue: string, rowCount: number, exact: boolean = false) {
    // Search
    this.findButton('Search').click();

    // Find cells
    const cells = $$(`mat-cell.${column}`);

    // Verfiy row ount
    if (exact) {
      expect(cells.count()).toEqual(rowCount);
    } else {
      expect(cells.count()).toBeGreaterThanOrEqual(rowCount);
    }

    // Verify cell value
    cells.each((cell) => {
      this.assertElementText(cell, cellValue);
    });
  }

  /**
   * Verfiy the cells are sorted.
   * @param column the column name
   * @param asc asc or desc order
   */
  private verifyCellsSorted(column: string, asc: boolean) {
    const cells = $$(`mat-cell.${column}`);
    cells.map(elm => elm.getText()).then((values: string[]) => {
      let sorted = true;
      for (let i = 0; i < values.length - 1; i++) {
        if ((asc && values[i + 1] === '') || (!asc && values[i] === '')) {
          continue;
        }

        if (column === 'mat-column-Timestamp') {
          const valuei = moment(values[i], 'M/D/YY, hh:mm A').unix();
          const valuei1 = moment(values[i + 1], 'M/D/YY, hh:mm A').unix();
          if ((asc && valuei > valuei1) || (!asc && valuei < valuei1)) {
            sorted = false;
            break;
          }
        } else {
          const valuei = values[i].toLowerCase();
          const valuei1 = values[i + 1].toLowerCase();
          if ((asc && valuei > valuei1) || (!asc && valuei < valuei1)) {
            sorted = false;
            break;
          }
        }
      }
      assert(sorted, `${column} is not sorted: ${JSON.stringify(values)}`);
    });
  }

  /**
   * Test sort.
   * @param column the column to sort on
   */
  testSort(column: string) {
    this.findButton('Reset Sort Order').click();
    this.closeToast();

    column = `mat-column-${column}`;

    // Sort Asc
    const header = $(`mat-header-cell.${column}`);
    header.click();

    this.verifyCellsSorted(column, true);

    // Go to next page
    $('button.mat-paginator-navigation-next').click();
    this.verifyCellsSorted(column, true);

    // Sort Desc
    header.click();
    this.verifyCellsSorted(column, false);

    // Go to previous page
    $('button.mat-paginator-navigation-previous').click();
    this.verifyCellsSorted(column, false);
  }
}
