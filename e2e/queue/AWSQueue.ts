/*
 * Copyright (C) 2018 TopCoder Inc., All Rights Reserved.
 */
import { SQS } from 'aws-sdk';

/**
 * AWS queue.
 *
 * @author TCSCODER
 * @version 1.0
 */
export class AWSQueue {

  /**
   * The queue name.
   */
  private name: string;

  /**
   * The queue url.
   */
  private queueUrl: string;

  /**
   * The queue.
   */
  private queue: SQS;

  /**
   * Constructor with queue name and region.
   * @param name the queue name
   * @param region the region
   */
  constructor(name: string, region: string) {
    this.name = name;

    this.queue = new SQS({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      region
    });
  }

  /**
   * Get queue url.
   * @returns queue url
   */
  async getQueueUrl(): Promise<string> {
    if (this.queueUrl) {
      return this.queueUrl;
    }

    const result = await this.queue.getQueueUrl({ QueueName: this.name }).promise();
    this.queueUrl = result.QueueUrl;
    return this.queueUrl;
  }

  /**
   * Send message.
   * @param message the message to send
   */
  async sendMessage(message): Promise<void> {
    const queueUrl = await this.getQueueUrl();

    await this.queue.sendMessage({
      QueueUrl: queueUrl,
      MessageBody: JSON.stringify(message)
    }).promise();
  }

  /**
   * Receive message.
   * @returns messages
   */
  async receiveMessage(): Promise<SQS.Message[]> {
    const queueUrl = await this.getQueueUrl();

    const result = await this.queue.receiveMessage({
      QueueUrl: queueUrl,
      MaxNumberOfMessages: 10,
      WaitTimeSeconds: 20 // Wait up to 20 seconds for message available
    }).promise();

    return result.Messages;
  }

  /**
   * Delete message.
   * @param receiptHandle the receipt handle of message to delete
   */
  async deleteMessage(receiptHandle: string): Promise<void> {
    const queueUrl = await this.getQueueUrl();

    await this.queue.deleteMessage({
      QueueUrl: queueUrl,
      ReceiptHandle: receiptHandle
    }).promise();
  }

  /**
   * Purge queue.
   */
  async purgeQueue(): Promise<void> {
    const queueUrl = await this.getQueueUrl();

    await this.queue.purgeQueue({
      QueueUrl: queueUrl
    }).promise();
  }
}
