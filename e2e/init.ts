/*
 * Copyright (C) 2018 TopCoder Inc., All Rights Reserved.
 */
import { browser } from 'protractor';
import { AWSQueue } from './queue/AWSQueue';

/**
 * Initialize E2E tests.
 *
 * @author TCSCODER
 * @version 1.0
 */
// Export the messages data
const sns = require('./data/sns.json');
const sns_invalidjson = require('./data/sns_invalidjson.json');
const sqs_async = require('./data/sqs_async.json');
const sqs_empty_uuid = require('./data/sqs_empty_uuid.json');
const sqs_invalid_tranmission_type = require('./data/sqs_invalid_tranmission_type.json');
const sqs_invalidstatus = require('./data/sqs_invalidstatus.json');
const sqs_sync = require('./data/sqs_sync.json');
const sqs_sery_dash_int = require('./data/sqs_sery_dash_int.json');
const sqs_sery_dash_int_nojson = require('./data/sqs_sery_dash_int_nojson.json');
export const data = {
  sns, sns_invalidjson, sqs_async, sqs_empty_uuid, sqs_invalid_tranmission_type, sqs_invalidstatus, sqs_sync,
  sqs_sery_dash_int, sqs_sery_dash_int_nojson
};

// Adjust data to set the Provider/Service to be some unique values.
const now = Date.now();

export const provider = `Provider ${now}`;
export const service = `Service ${now}`;
export const version = 'Version';

sqs_async.dashboard_wrapper.service_provider_name = `${provider} 1`;
sqs_async.dashboard_wrapper.message.header.service_name = `${service} 1`;
sqs_async.dashboard_wrapper.message.header.service_version = `${version} 1`;

sqs_empty_uuid.dashboard_wrapper.service_provider_name = `${provider} 1`;
sqs_empty_uuid.dashboard_wrapper.message.header.service_name = `${service} 2`;
sqs_empty_uuid.dashboard_wrapper.message.header.service_version = `${version} 2`;

sqs_invalid_tranmission_type.dashboard_wrapper.service_provider_name = `${provider} 2`;
sqs_invalid_tranmission_type.dashboard_wrapper.message.header.service_name = `${service} 3`;
sqs_invalid_tranmission_type.dashboard_wrapper.message.header.service_version = `${version} 3`;

sqs_invalidstatus.dashboard_wrapper.service_provider_name = `${provider} 2`;
sqs_invalidstatus.dashboard_wrapper.message.header.service_name = `${service} 4`;
sqs_invalidstatus.dashboard_wrapper.message.header.service_version = `${version} 4`;

sqs_sync.dashboard_wrapper.service_provider_name = `${provider} 3`;
sqs_sync.dashboard_wrapper.service_name = `${service} 5`;
sqs_sync.dashboard_wrapper.service_version = `${version} 5`;

sns.header.publishing_system = `${provider} 4`;
sns.header.message_name = `${service} 6`;
sns.header.message_version = `${version} 6`;

sns_invalidjson.header.publishing_system = `${provider} 4`;
sns_invalidjson.header.message_name = `${service} 7`;
sns_invalidjson.header.message_version = `${version} 7`;

sqs_sery_dash_int.dashboard_wrapper.message_type = `${service} 8`;
sqs_sery_dash_int.dashboard_wrapper.service_version = `${version} 8`;

sqs_sery_dash_int_nojson.dashboard_wrapper.message_type = `${service} 9`;
sqs_sery_dash_int_nojson.dashboard_wrapper.service_version = `${version} 9`;

// Export the config
const regionName = process.env.AWS_REGION;
const queue1Name = process.env.QUEUE_1_SQS_NAME;
const deadQueueName = process.env.REPLAY_QUEUE_SQS_NAME;

console.log(`Use Region name: ${regionName}`);
console.log(`Use Queue1 name: ${queue1Name}`);
console.log(`Use Dead Queue name: ${deadQueueName}`);

export const config = {
  regionName,
  queue1Name,
  deadQueueName,
  REPLAY_USER: 'Michael Lee',
  READONLY_USER: 'Dave Mack'
};

let setupDone = false;
/**
 * Setup data. Deliver messages to AWS queue.
 */
export async function setupData(): Promise<void> {
  if (setupDone) {
    return;
  }

  const queue = new AWSQueue(queue1Name, regionName);

  for (const key of Object.keys(data)) {
    const msg = data[key];
    await queue.sendMessage(msg);
    console.log(`Wait 2 seconds for message to be delivered: ${JSON.stringify(msg)}`);
    await browser.driver.sleep(2000);
  }

  setupDone = true;
  console.log('Setup finished');
}
