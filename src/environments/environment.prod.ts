export const environment = {
  production: true,
  baseUri: '$BACKEND_SERVER_URL',
  pageSizeOptions: [5, 10, 20, 50]
};
