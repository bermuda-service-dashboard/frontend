import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Sort, MatSort } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/withLatestFrom';

import * as _ from 'lodash';

import { environment } from '../../../environments/environment';

import { DataService } from '../../services/data.service';
import { ServiceDataSource } from '../../services/service-data-source';

@Component({
  selector: 'app-sd-dashboard',
  templateUrl: 'serviceSearch.component.html',
  styleUrls: ['serviceSearch.component.scss']
})

export class ServiceSearchComponent implements OnInit {
  displayedColumns = ['Provider', 'Service', 'Version'];
  dataSource: ServiceDataSource;
  serviceProviders: any;
  serviceNames: any;
  serviceVersions: any;

  // input events:
  provider$: Subject<any> = new Subject; // service provider selected
  service$: Subject<any> = new Subject; // service selected

  providerId: number;
  serviceName: string;
  versionId: number;
  sortOrder = '';
  sortBy = '';
  searchParams: any;

  directionMap = { asc: 'ASC', desc: 'DESC'};

  pageSizeOptions: number[] = environment.pageSizeOptions;

  constructor (
    private dataService: DataService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit () {
    this.serviceProviders = this.dataService.getServiceProviders();

    // combine input event stream with last data from api, and map that to services
    const services$ = this.dataService.getServices();
    this.serviceNames = this.provider$.withLatestFrom(services$)
      .map(([event, services]) => (
        event && services.filter(({ provider }) => provider === event.value)));

    // map input event to the selected service object
    const versions$ = this.dataService.getServiceVersions();
    const service$ = this.service$.withLatestFrom(services$)
      .map(([ev, services]) => ev && services.find(({ label }) => label === ev.value));

    // combine input event stream with last data from api, and map that to serviceVersions
    this.serviceVersions = service$.withLatestFrom(versions$)
      .map(([service, versions]) => (
        service && versions.filter(({ serviceId }) => serviceId === service.id)));

    this.search();
  }

  /**
   * change service data to table data
   * @param item
   * @return {{provider: any, service, version: any}}
   */
  transfromTableData(item) {
    return {
      provider: item.sdServiceProvider && item.sdServiceProvider.name,
      service: item.name,
      version: item.sdServiceVersion && item.sdServiceVersion.version,
    };
  }

  /**
   * prepare params
   */
  prepareParams() {
    if (this.providerId) {
      this.searchParams['serviceProviderId'] = this.providerId;
    }
    if (this.serviceName && this.serviceName !== '') {
      this.searchParams['name'] = this.serviceName;
    }
    if (this.versionId) {
      this.searchParams['serviceVersionId'] = this.versionId;
    }
    if (this.sortBy && this.sortOrder && this.sortBy !== '' && this.sortOrder !== '') {
      this.searchParams['sortBy'] = this.sortBy;
      this.searchParams['sortOrder'] = this.sortOrder.toUpperCase();
    }
  }

  /**
   * search service
   */
  search(options = {}) {
    this.searchParams = {};
    this.prepareParams();
    const params: any = {};

    if (this.dataSource) {
      params.limit = this.dataSource.getPageSize();
      params.offset = this.dataSource.getIndex() * params.limit;
    }

    this.dataService.searchService({ ...params, ...this.searchParams, ...options }).subscribe((data: ServiceDataSource) => {
      this.dataSource = data;

      // notify if no results were found
      if (data.getLength() === 0) {
        this.toastr.info('No results found');
      }
    });
  }

  /**
   * clear search params
   */
  clear() {
    this.providerId = null;
    this.serviceName = null;
    this.versionId = null;

    this.serviceNames.next(null);
    this.serviceVersions.next(null);
  }
  /**
   * reset sort order
   */
  resetSortOrder() {
    this.sortBy = '';
    this.sortOrder = '';
    this.toastr.info('reset success');

    this.search();
  }

  /**
   * set sort order
   * @param {Sort} sort
   */
  sortData(sort: Sort) {
    this.sortBy = sort.active;
    this.sortOrder = sort.direction;

    this.search();
  }

  /**
   * page change
   * @param event event of page change
   */
  onPaginateChange(event) {
    const offset = event.pageIndex * event.pageSize;
    const limit = event.pageSize;

    this.search({ offset, limit });
  }
}
