import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';

import { Sort } from '@angular/material';
import { DataService } from '../../services/data.service';
import { ToastsManager } from 'ng2-toastr';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ScalarObservable } from 'rxjs/observable/ScalarObservable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/withLatestFrom';

import * as _ from 'lodash';
import * as moment from 'moment';

import { environment } from '../../../environments/environment';
import { MessageDataSource } from '../../services/message-data-source';
import { MessageSchema } from '../../services/message-schema';
import { ReplayPopupComponent } from '../../components/replay-popup/replay-popup.component';
import { DisplayMessageComponent } from '../../components/display-message/display-message.component';

const ATTRIBUTE = { value: '', name: '' };

// parse date & timestamp fields and set them as ISO dates
const parseDates = (obj) => {
  _.forOwn(obj, (value, key) => {
    if (typeof value === 'object') {
      obj[key] = parseDates(value);
    }

    if (/date|timestamp/i.test(key)) {
      obj[key] = (new Date(obj[key])).toISOString();
    }
  });

  return obj;
};

@Component({
  selector: 'app-sd-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  displayedColumns = [
    'Select',
    'Provider',
    'Service',
    'Version',
    'Sync/Async',
    'Type',
    'UserID',
    'RequestingSystem',
    'RequestingSystemMessage',
    'Status',
    'Replay',
    'Timestamp'
  ];

  dataSource: MessageDataSource;

  // input events:
  provider$: Subject<any> = new Subject; // service provider selected
  service$: Subject<any> = new Subject; // service selected

  // select options
  serviceProviders: any;
  serviceNames: any;
  serviceVersions: any;
  messageTypes: any;
  // store the values
  messageTypes$: any;
  requestingSystems: any;
  transmissionTypes: any;
  statuses: any;

  replayTypes = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];

  providerId: number;
  serviceName: string;
  messageTypeId: number;
  replayType: boolean;
  transmissionType: string;
  statusCd: string;
  requestingSystemId: number;
  requestingSystemMessageId: number;
  correlationId: string;
  versionId: number;
  startDt: number;
  endDt: number;
  userId: number;

  attrs: any[] = [];

  sortOrder = '';
  sortBy = '';
  searchParams: any;

  pageSizeOptions: number[] = environment.pageSizeOptions;
  canReplay: any;

  constructor (
    private dataService: DataService,
    public toastr: ToastsManager,
    private dialog: MatDialog,
    vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit () {
    this.serviceProviders = this.dataService.getServiceProviders();

    // combine input event stream with last data from api, and map that to services
    const services$ = this.dataService.getServices();
    this.serviceNames = this.provider$.withLatestFrom(services$)
      .map(([event, services]) => (
        event && services.filter(({ provider }) => provider === event.value)));

    // map input event to the selected service object
    const versions$ = this.dataService.getServiceVersions();
    const service$ = this.service$.withLatestFrom(services$)
      .map(([ev, services]) => ev && services.find(({ label }) => label === ev.value));

    // combine input event stream with last data from api, and map that to serviceVersions
    this.serviceVersions = service$.withLatestFrom(versions$)
      .map(([service, versions]) => (
        service && versions.filter(({ serviceId }) => serviceId === service.id)));

    this.messageTypes = this.dataService.getMessageTypes();
    this.statuses = this.dataService.getStatuses();
    this.transmissionTypes = this.dataService.getTransmissionType();
    this.requestingSystems = this.dataService.getRequestingSystems();

    this.canReplay = this.dataService.getUserRole()
      .map((d: any) => d.indexOf('REPLAY') > -1)
      .share();

    this.search();

    this.messageTypes.subscribe(types => this.messageTypes$ = types);
    this.addAttr();
  }

  addAttr() {
    const attrs = _.flattenDeep(this.attrs);
    attrs.push({ ...ATTRIBUTE }, { ...ATTRIBUTE });
    this.attrs = _.chunk(attrs, 2);
  }

  /**
   * clear the input values
   */
  clear() {
    this.providerId = null;
    this.messageTypeId = null;
    this.replayType = null;
    this.transmissionType = null;
    this.statusCd = null;
    this.requestingSystemId = null;
    this.requestingSystemMessageId = null;
    this.correlationId = null;
    this.versionId = null;
    this.serviceName = null;
    this.userId = null;
    this.startDt = null;
    this.endDt = null;

    this.serviceNames.next(null);
    this.serviceVersions.next(null);

    _.forEach(this.attrs, attrArray => {
      _.forEach(attrArray, attr => {
        attr.value = '';
        attr.name = '';
      });
    });
  }

  /**
   * get message type by id
   * @param {[type]} messageId [description]
   */
  getMessageTypeById(messageId) {
    const types = _.filter(this.messageTypes$ || [], item => item.value === messageId);
    return types[0] && types[0].label;
  }

  addDynamicAttr(name, value) {
    if (name !== '' && value !== '') {
      if (!this.searchParams['attributeSearchCriteria.attributes']) {
        this.searchParams['attributeSearchCriteria.attributes'] = [];
      }
      this.searchParams['attributeSearchCriteria.attributes'].push({ name, value });
    }
  }

  /**
   * prepare params to filter messages
   */
  prepareParams() {
    if (this.providerId) {
      this.searchParams['serviceProviderId'] = this.providerId;
    }
    if (this.versionId) {
      this.searchParams['serviceVersionId'] = this.versionId;
    }
    if (this.serviceName && this.serviceName !== '') {
      this.searchParams['serviceName'] = this.serviceName;
    }
    if (this.messageTypeId) {
      this.searchParams['messageTypeId'] = this.messageTypeId;
    }
    if (this.replayType !== null && this.replayType !== undefined) {
      this.searchParams['replayMessage'] = this.replayType;
    }
    if (this.userId) {
      this.searchParams['userId'] = this.userId;
    }
    if (this.requestingSystemId) {
      this.searchParams['requestingSystemId'] = this.requestingSystemId;
    }
    if (this.requestingSystemMessageId) {
      this.searchParams['requestingSystemMessageId'] = this.requestingSystemMessageId;
    }
    if (this.transmissionType) {
      this.searchParams['transmissionType'] = this.transmissionType;
    }
    if (this.statusCd) {
      this.searchParams['statusCd'] = this.statusCd;
    }
    if (this.startDt) {
      this.searchParams['startDate'] = moment(this.startDt).format('YYYY-MM-DD');
    }
    if (this.endDt) {
      this.searchParams['stopDate'] = moment(this.endDt).format('YYYY-MM-DD');
    }
    if (this.correlationId && this.correlationId !== '') {
      this.searchParams['coorelationUuid'] = this.correlationId;
    }
    if (this.sortBy && this.sortOrder && this.sortBy !== '' && this.sortOrder !== '') {
      this.searchParams['sortBy'] = this.sortBy;
      this.searchParams['sortOrder'] = this.sortOrder.toUpperCase();
    }

    _.forEach(this.attrs, attrArray => {
      _.forEach(attrArray, attr => {
        this.addDynamicAttr(attr.name, attr.value);
      });
    });
  }

  /**
   * search message
   */
  search(options = {}) {
    this.searchParams = {};
    this.prepareParams();

    const params: any = {};

    if (this.dataSource) {
      params.limit = this.dataSource.getPageSize();
      params.offset = this.dataSource.getIndex() * params.limit;
    }

    this.dataService.searchMessage({ ...params, ...this.searchParams, ...options })
    .subscribe((data: MessageDataSource) => {
      this.dataSource = data;

      // notify if no results were found
      if (!data.getLength()) {
        this.toastr.info('No results found');
      }
    });
  }

  /**
   * set sort order
   * @param {Sort} sort
   */
  sortData(sort: Sort) {
    this.sortBy = sort.active;
    this.sortOrder = sort.direction;

    this.search();
  }

  /**
   * reset sort order
   */
  resetSortOrder() {
    this.sortBy = '';
    this.sortOrder = '';
    this.toastr.info('reset success');

    this.search();
  }

  /**
   * page change
   * @param event event of page change
   */
  onPaginateChange(event) {
    const offset = event.pageIndex * event.pageSize;
    const limit = event.pageSize;

    this.search({ offset, limit });
  }

  /**
   * replay message
   *
   * @param {any} data {messageIds, replayEndpoint, replayEndpointType}
   */
  replayMessage(data) {
    this.dataService.replayMessage(data)
      .catch((error) => {
        this.toastr.error('can not replay these message');
        return Observable.of({error: true});
      })
      .subscribe((response: any) => {
        if (response && response.error) {
          return;
        }

        const sourceData = <ScalarObservable<MessageSchema[]>> this.dataSource.connect();
        _.forEach(sourceData.value, item => {
          item.selected = false;
        });

        this.toastr.info('replay success');
      });
  }

  /**
   * getSelectedMessages Get the user selected messages
   */
  getSelectedMessages() {
    if (!this.dataSource || !this.dataSource.connect) {
      return [];
    }

    const data = <ScalarObservable<MessageSchema[]>> this.dataSource.connect();
    return data.value.filter(item => item.selected === true);
  }

  /**
   * confirmReplay Confrim the message replay - show a popup, then send the replay request
   */
  confirmReplay() {
    const messages = this.getSelectedMessages();
    const messageIds = messages.map(item => item.messageId);

    // no message selected, show a toast message
    if (messages.length === 0) {
      this.toastr.info('please select some message');
      return;
    }

    // create the dialog/popup and open it
    const dialog = this.dialog.open(ReplayPopupComponent, {
      data: { messages },
    });

    // on close check if user confirmed
    dialog.afterClosed().subscribe(data => {
      if (!data) {
        return;
      }

      // user confirmed, pass data to replay request
      this.replayMessage({ ...data, messageIds });
    });
  }

  /**
   * displayMessage Display the selected message in a popup as a JSON
   * and allow user to copy it to clipboard
   */
  displayMessage() {
    const data = _.cloneDeep(this.getSelectedMessages()[0]);
    let { bmessage } = data;

    try {
      // parse the bmessage
      bmessage = JSON.parse(data.bmessage);
    } catch (e) {
      /* nothing needs to be done here, the bmessage defaults to the unparsed json string*/
    }

    // modify the message for better data visualization
    const message = {
      ...data,
      bmessage,
      // remove fields from visualization
      sdServiceProvider: undefined,
      sdServiceVersion: undefined,
      sdService: undefined,
      sdStatusCd: undefined,
      sdMessageType: undefined,
      sdRequestingSystem: undefined,
      serviceId: undefined,
      serviceProviderId: undefined,
      provider: undefined,
      status: undefined,
      // update lookup values, handle undefined props
      service: (data.sdService || {}).name || data.service,
      serviceProvider: (data.sdServiceProvider || {}).name || data.provider,
      requestingSystem: (data.sdRequestingSystem || {}).name,
      messageType: (data.sdMessageType || {}).description,
      statusCd: (data.sdStatusCd || {}).description || data.status,
    };

    // convert dates and timestamps to ISO date
    parseDates(message);

    // create the dialog/popup and open it
    const dialog = this.dialog.open(DisplayMessageComponent, {
      data: { message },
    });

    dialog.afterClosed().subscribe(ev => {
      if (!ev) {
        return;
      }

      this.toastr.info('Message copied to clipboard!');
    });
  }
}
