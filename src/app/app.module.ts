import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClipboardModule } from 'ngx-clipboard';
import { Md2Module, MATERIAL_COMPATIBILITY_MODE } from 'md2';

import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatSortModule,
  MatDialogModule,
} from '@angular/material';

import { AppComponent } from './app.component';

// routing module
import { AppRoutingModule } from './app-routing.module';

// pages
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ServiceSearchComponent } from './pages/serviceSearch/serviceSearch.component';

// components
import { HeaderComponent } from './components/header/header.component';

// services
import { DataService } from './services/data.service';
import { ReplayPopupComponent } from './components/replay-popup/replay-popup.component';
import { DisplayMessageComponent } from './components/display-message/display-message.component';

/**
 * Custom Toast Options
 */
export class CustomToastOption extends ToastOptions {
  positionClass = 'toast-bottom-right';
  newestOnTop = true;
  showCloseButton = true;
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    ServiceSearchComponent,
    ReplayPopupComponent,
    DisplayMessageComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    Md2Module,
    ClipboardModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    AppRoutingModule,
    MatTableModule,
    MatNativeDateModule,
    MatPaginatorModule,
    FormsModule,
    MatCheckboxModule,
    MatSortModule,
    MatDialogModule,
    ToastModule.forRoot(),
  ],
  entryComponents: [
    ReplayPopupComponent,
    DisplayMessageComponent,
  ],
  providers: [
    DataService,
    { provide: MATERIAL_COMPATIBILITY_MODE, useValue: true },
    { provide: ToastOptions, useClass: CustomToastOption },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
