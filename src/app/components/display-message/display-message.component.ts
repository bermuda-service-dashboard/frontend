import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-display-message',
  templateUrl: './display-message.component.html',
  styleUrls: ['./display-message.component.css']
})
export class DisplayMessageComponent {
  constructor(public ref: MatDialogRef<DisplayMessageComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}
}
