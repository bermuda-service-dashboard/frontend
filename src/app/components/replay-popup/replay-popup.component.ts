import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import * as _ from 'lodash';

@Component({
  selector: 'app-replay-popup',
  templateUrl: './replay-popup.component.html',
  styleUrls: ['./replay-popup.component.css']
})
export class ReplayPopupComponent implements OnInit {
  endpoint = '';
  endpointType = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    const { messages } = this.data;

    this.endpoint = messages[0].replayEndpoint;
    this.endpointType = messages[0].replayEndpointType;
  }
}
