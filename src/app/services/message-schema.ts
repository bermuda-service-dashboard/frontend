export interface MessageSchema {
  messageId: number;
  provider: string;
  service: string;
  version: number;
  transmissionType: string;
  type: string;
  userId: number;
  status: string;
  replay: string;
  timestamp: string;
  selected: boolean;
}
