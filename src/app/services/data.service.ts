import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';

import { SessionService } from './session.service';
import { environment } from '../../environments/environment';
import { MessageDataSource } from './message-data-source';
import { MessageSchema } from './message-schema';
import { ServiceDataSource } from './service-data-source';
import { ServiceSchema } from './service-schema';

import * as _ from 'lodash';

const { baseUri } = environment;

/* service for enabled data */
@Injectable()
export class DataService {
  constructor (private http: HttpClient) {}

  /**
   * getAuthHeaders Get headers for authenticating current user
   *
   * @return auth headers for request
   */
  getAuthHeaders() {
    const username = SessionService.get('username');
    return { headers: new HttpHeaders({username}) };
  }

  /**
   * getServiceProviders Get service providers from api
   *
   * @return {Observable<any>}
   */
  getServiceProviders() {
    return this.http.get(`${baseUri}/lookup/serviceProviders`, this.getAuthHeaders())
      .map((data: any) => data.map(item => ({ value: item.serviceProviderId, label: item.name })))
      .catch(this.handleError).share();
  }

  /**
   * get serviceProviders
   * @return {Observable<any>}
   */
  getServices () {
    return this.http.get(`${baseUri}/services`, { params: this.transformHttpParams({limit: 2147483647}), ...this.getAuthHeaders() })
      .map((data: any) => data.results.map(item => ({
        id: item.serviceId,
        value: item.name,
        label: item.name,
        provider: item.serviceProviderId,
      })))
      .catch(this.handleError).share();
  }

  /**
   * get services
   * @return {Observable<any>}
   */
  getServiceVersions () {
    return this.http.get(`${baseUri}/lookup/serviceVersions`, this.getAuthHeaders())
      .map((data: any) => data.map(item => ({
        value: item.serviceVersionId,
        label: item.version,
        serviceId: item.serviceId,
      })))
      .catch(this.handleError).share();
  }

  /**
   * get requesting system
   * @return {Observable<any>}
   */
  getRequestingSystems () {
    return this.http.get(`${baseUri}/lookup/requestingSystems`, this.getAuthHeaders())
      .map((data: any) => data.map(item => ({ value: item.requestingSystemId, label: item.name })))
      .catch(this.handleError).share();
  }

  /**
   * get transmission type
   * @return {Observable<any>}
   */
  getTransmissionType () {
    return this.http.get(`${baseUri}/lookup/transmissionTypes`, this.getAuthHeaders())
      .map((data: any) => _.values(data).map(value => ({ value, label: value })))
      .catch(this.handleError).share();
  }

  /**
   * get statuses type
   * @return {Observable<any>}
   */
  getStatuses () {
    return this.http.get(`${baseUri}/lookup/statuses`, this.getAuthHeaders())
      .map((data: any) => data.map(item => ({ value: item.statusCd, label: item.description })))
      .catch(this.handleError).share();
  }

  /**
   * get message Types
   * @return {Observable<any>}
   */
  getMessageTypes () {
    return this.http.get(`${baseUri}/lookup/messageTypes`, this.getAuthHeaders())
      .map((data: any) => data.map(item => ({ value: item.messageTypeId, label: item.description })))
      .catch(this.handleError).share();
  }

  /**
   * search service
   * @param params
   * @return {Observable<ServiceDataSource>}
   */
  searchService (params): Observable<ServiceDataSource> {
    return this.http.get(`${baseUri}/services`, { params, ...this.getAuthHeaders() })
      .catch(({ error }) => {
        console.error(error.message || error);
        return Observable.of({results: []});
      })
      .map((data: any) => {
        const array: ServiceSchema[] = _.map(data.results, item => ({
          provider: item.sdServiceProvider && item.sdServiceProvider.name,
          service: item.name,
          version: item.sdServiceVersion && item.sdServiceVersion.version,
        }));

        return new ServiceDataSource(array, data.offset / data.limit, data.limit, data.total);
      }).share();
  }

  /**
   * transformHttpParams Convert object to url params
   * @param  {any}     params
   *
   * @return {HttpParams}
   */
  transformHttpParams(params): HttpParams {
    let httpParams = new HttpParams();

    _.forEach(_.keys(params), key => {
      if (_.isArray(params[key])) {
        for (let i = 0; i < params[key].length; i += 1) {
          if (_.isObject(params[key][i])) {
             _.forEach(_.keys(params[key][i]), name => {
               httpParams = httpParams.append(`${key}[${i}].${name}`, params[key][i][name]);
             });
          }
        }
      } else {
        httpParams = httpParams.append(key, params[key]);
      }
    });

    return httpParams;
  }

  /**
   * change message data to table data
   * @param {any} item
   */
  transformTableData(item) {
    return {
      ...item,
      messageId: item.transactionId,
      provider: item.sdServiceProvider && item.sdServiceProvider.name,
      service: item.sdService && item.sdService.name,
      version: item.serviceVersion,
      transmissionType: item.transmissionType,
      typeId: item.messageTypeId,
      userId: item.userId,
      status: item.statusCd,
      replay: item.replayMessage === true ? 'Yes' : 'No',
      timestamp: item.processingTimestamp,
      selected: false,
    };
  }

  /**
   * search message
   * @param params
   *
   * @return {Observable<MessageDataSource>}
   */
  searchMessage (params): Observable<MessageDataSource> {
    const httpParams = this.transformHttpParams(params);

    return this.http.get(`${baseUri}/messages?${httpParams.toString()}`, this.getAuthHeaders())
      .catch(({ error }) => {
        console.error(error.message || error);
        return Observable.of({results: []});
      })
      .map((data: any) => {
        const array: MessageSchema[] = _.map(data.results, item => this.transformTableData(item));
        return new MessageDataSource(array, data.offset / data.limit, data.limit, data.total);
      });
  }

  /**
   * replay message by message ids
   * @param body
   *
   * @return {Promise<Response>}
   */
  replayMessage (body) {
    return this.http.post(`${baseUri}/messages/replay`, body, this.getAuthHeaders());
  }

  /**
   * getUserRole Get role for current user
   */
  getUserRole() {
    return this.http.get(`${baseUri}/user`, this.getAuthHeaders()).share();
  }

  /**
   * get error message
   * @param {[number]} err error status
   */
  getErrorMessage (err) {
    try {
      const errJson = err.json();
      if (errJson && errJson.message) {
        return errJson.message;
      }
      return JSON.stringify(err);
    } catch (e) {
      return err.message;
    }
  }

  /**
   * common handle error
   * @param  {any}          error
   *
   * @return {Observable}
   */
  private handleError (error: any) {
    console.error(error.statusText || error);
    return Observable.of([]);
  }
}
