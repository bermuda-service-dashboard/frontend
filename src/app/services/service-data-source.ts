import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ServiceSchema } from './service-schema';

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class ServiceDataSource extends DataSource<any> {
  constructor(
    private data: any[],
    private pageIndex: number,
    private pageSize: number,
    private length: number
  ) {
    super();
  }
  connect(): Observable<ServiceSchema[]> {
    return Observable.of(this.data);
  }

  disconnect() {}

  /**
   * getLength Get length of data
   */
  getLength() {
    return this.length;
  }

  /**
   * getPageSize Get current page size
   */
  getPageSize() {
    return this.pageSize;
  }

  /**
   * getIndex Get current page index
   */
  getIndex() {
    return this.pageIndex;
  }
}
