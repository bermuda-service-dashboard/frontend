const key = 'sv-dash-session';
const session = JSON.parse(sessionStorage.getItem(key) || '{}');

/**
 * SessionService Keep session data, stores it in sessionStorage
 * @type {Object}
 */
export const SessionService = {
  set(prop: string, value: any) {
    session[prop] = value;
    sessionStorage.setItem(key, JSON.stringify(session));

    return this;
  },

  get(prop: string) {
    return session[prop];
  }
};
