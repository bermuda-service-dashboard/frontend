export interface ServiceSchema {
  provider: string;
  service: string;
  version: string;
}
